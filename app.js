var sessionMinutes = window.sessionStorage.getItem("minTime");
var sessionSeconds = window.sessionStorage.getItem("secondsTime");
var minutes = sessionMinutes ? sessionMinutes : 0;
var seconds = sessionSeconds ? sessionSeconds : 0;

var aMinutes = document.getElementById("mins");
var aSeconds = document.getElementById("seconds");
var startBtn = document.getElementById("start");
var pauseBtn = document.getElementById("pause");
var resetBtn = document.getElementById("reset");
var Interval;

startBtn.onclick = function () {
  clearInterval(Interval);
  Interval = setInterval(startTimer, 1000);
};

pauseBtn.onclick = function () {
  clearInterval(Interval);
  sessionStorage.setItem("minTime", minutes);
  sessionStorage.setItem("secondsTime", seconds);
};

resetBtn.onclick = function () {
  minutes = 00;
  seconds = 00;
  clearInterval(Interval);
  minutes = minutes.toString().padStart(2, "0");
  seconds = seconds.toString().padStart(2, "0");

  aMinutes.innerHTML = minutes;
  aSeconds.innerHTML = seconds;
  window.sessionStorage.clear();
};

function startTimer() {
  seconds++;
  // aSeconds.innerHTML = "0" + seconds;
  // aMinutes.innerHTML = "0" + minutes;

  if (seconds > 59) {
    minutes++;
    seconds = 0;
  }
  seconds = seconds.toString().padStart(2, "0");
  aSeconds.innerHTML = seconds;
  minutes = minutes.toString().padStart(2, "0");
  aMinutes.innerHTML = minutes;
}
